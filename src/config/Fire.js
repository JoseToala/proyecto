import firebase from 'firebase';

const firebaseConfig = { 
    apiKey: "AIzaSyCWC07EnB8fsLKumaeClq1aO_1yVBhSQnQ",
    authDomain: "reactjs-678c1.firebaseapp.com",
    databaseURL: "https://reactjs-678c1.firebaseio.com",
    projectId: "reactjs-678c1",
    storageBucket: "",
    messagingSenderId: "438275444978",
    appId: "1:438275444978:web:61ae7597411ced29"
};
const fire = firebase.initializeApp(firebaseConfig);
export default fire;